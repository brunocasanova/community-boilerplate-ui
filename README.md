# Community Boilerplate UI

_Repository for a real-time community centered demo-application._

PWA UI repository to consume data from an API.

It's a `WIP` and some of the tecnologies/libraries that integrates are:
- React
- Redux
- Redux-thunk
- Connected-react-router
- Jsonwebtoken
- Socket.io-client
- Primereact (theming)
- Styled-components

### Some application functionalities:

- The application have a authentication system using JWT system and validation for authorizations.
- Users can add other users as connections and they can be a part of a community and create one and add other users aswell.
- Users can message eachother if they are connected.
- Users receive notifications when some action happens in real time.
- Search system for finding users, communities by its informations.
- Application detects the devices and logs to the users the last login they made and with what device they've used.
- Community owners can communicate with the users that are a part of their communities by announcements.
- Email system for account confirmation and recovering passwords.

## Initialize:

#### Developing mode:

`npm run dev`

_Watches all changes made to files and reloads after any changes._

#### Production mode:

`npm run build`

_Make a build of the project for a ready to use application._

_Create a `.env` file using the example like the `.example.env` file for your specific configurations._

## TODOs:

## Other infos:

- This project was made by Bruno Casanova and is still a work in progress and you can use it how you want for your projects.

- If you want to contribute for the project feel free to do so and leave a PR with a message and ill be happy to assist you.



