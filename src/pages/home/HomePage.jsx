import React from "react"
import LayoutComponent from "../../components/layout/LayoutComponent"
import LoginComponent from "../../components/LoginComponent/LoginComponent"
import { Link } from "react-router-dom"
import logo from "../../assets/images/logo-white.png"
import HomePageWrapper from "./HomePageWrapper"
import { Button } from "primereact/button"

const HomePage = props => {
  return (
    <LayoutComponent>
      <HomePageWrapper>
        <div className="home-header">
          <div className="home-title">
            <h1 className="share-tech-mono primary">
              Welcome to React Node Mongo App
            </h1>
            <h4>
              Please log in or <Link to="/register">create</Link> a new account
              to gain further access.
            </h4>
          </div>
        </div>

        <div className="home-logo">
          <img src={logo} alt="logo" />
        </div>

        <div className="home-text">
          <h3 className="share-tech-mono"></h3>
        </div>

        <div className="home-links">
          <h3>Other Links</h3>
        </div>

        <div className="login">
          <LoginComponent {...{ props }} />
        </div>

        <div className="home-social p-text-center">
          <h3 className="share-tech-mono">Other Networks</h3>
          <Button label="YouTube" className="p-button-link p-button-text" />
          <Button label="Twitter" className="p-button-link p-button-text" />
        </div>
      </HomePageWrapper>
    </LayoutComponent>
  )
}

export default HomePage
