import BasicInformationPanel from "./BasicInformationPanel"
import LocationPanel from "./LocationPanel"
import RibbonsPanel from "./RibbonsPanel"

export {
  BasicInformationPanel,
  LocationPanel,
  RibbonsPanel,
}
