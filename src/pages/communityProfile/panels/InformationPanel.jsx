import React from "react"
import { Card } from "primereact/card"
import {
  TextField,
  BooleanField,
  SimpleListField,
  DoubleTextField,
  ElementsWrapperSimple,
} from "../../../components/elements/PanelElements"
import { Link } from "react-router-dom"

const InformationPanel = ({ data }) => {
  const {
    owner,
    count,
    description,
  } = data

  return (
    <Card>
      <div className="profile-title-separator share-tech-mono">
        <h3 style={{ marginTop: "0px" }}>Information</h3>
      </div>
      <div className="profile-row">
        <div className="profile-column">
          {owner ? (
            <ElementsWrapperSimple>
              <p className="option share-tech-mono primary">Owner</p>
              <p className="detail">
                <Link className="primary" to={`/user/${owner.alias}`}>
                  {owner.displayName}
                </Link>
              </p>
            </ElementsWrapperSimple>
          ) : null}
          {TextField(description, "Description")}
        </div>
        <div className="profile-column">
          {DoubleTextField({
            field1: {
              value: count,
              title: "Members",
            },
          })}
          {TextField(requirementsToJoin, "Requirements to join")}
        </div>
      </div>

    </Card>
  )
}

export default InformationPanel
