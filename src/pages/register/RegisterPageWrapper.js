import styled from "styled-components"

export const RegisterPageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  margin-top: 50px;

  .register-logo{
    img{
      width: 120px;
      height: 120px;
    }

  }
`