import React from "react"
import RegisterComponent from "../../components/RegisterComponent/RegisterComponent"
import { RegisterPageWrapper } from "./RegisterPageWrapper.js"
import logo from "../../assets/images/logo-white.png"

const RegisterPage = props => {
  return (
    <RegisterPageWrapper>
      
      <div className="register-logo">
        <img src={logo} alt="logo" />
      </div>

      <h1 className="share-tech-mono">Create Account</h1>
      <RegisterComponent {...{ props }} />
    </RegisterPageWrapper>
  )
}

export default RegisterPage
