import React, { useState, useEffect } from "react"
import { InputText } from "primereact/inputtext"
import { Password } from "primereact/password"
import { Button } from "primereact/button"
import { useDispatch } from "react-redux"
import { create } from "../../store/actions/user"
import { RegisterComponentWrapper } from "./RegisterComponentWrapper"

import StartPanel from "./panels/start"

const initialState = {
  username: "",
  password: "",
  email: "",
}

const RegisterComponent = ({ props }) => {
  const dispatch = useDispatch()
  const [data, setData] = useState(initialState)

  const handleChange = e => {
    const { name, value } = e.target

    setData({ ...data, [name]: value })
  }

  const submitRegister = async e => {
    if (e.keyCode && e.keyCode !== 13) {
      return
    }

    if (data.username === "" && data.password === "" && data.email === "") {
      return
    }

    const response = await dispatch(create(data))

    if (response) {
      props.history.push("/confirm-email")
    }
  }

  return <RegisterComponentWrapper><StartPanel /></RegisterComponentWrapper>
}

export default RegisterComponent
