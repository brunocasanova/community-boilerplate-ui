import React from "react"
import { Button } from "primereact/button"

export default ({ props }) => {

  const handleClick = () =>{
    console.log()
  }

  return <>
    <div className="start-buttons">
      <Button label="Read Terms of Use" className="p-button-outlined" onClick={handleClick} />
    <Button label="I Accept" className="p-button-outlined" onClick={handleClick} />
    </div>

    <div className="start-info">
      <p>Warning! If you don't log into your account at least once every three months, your account will be automatically deleted.</p>
    </div>
  </>
}

